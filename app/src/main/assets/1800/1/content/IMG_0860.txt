In winter it gets very cold, down to -30 degrees. This can leave frost in the morning, like these ice crystals on the summerhouse window.

Image copyright David McGookin (2015)