HS1,1,60.185786,24.883539,30.0,Cafe Meritz,boathouse.jpg;boathouse.txt
HS2,1,60.185778,24.884427,30.0,Queue for Cafe Meritz,boathouse_pier_queue.jpg;boathouse_pier_queue.txt
HS3,1,60.176643,24.885078,30.0,The Ferry,folson_ferry.jpg;folson_ferry.txt
HS4,1,60.177141,24.882594,30.0,Inside the Boathouse,boathouse_interior.jpeg;boathouse_interior.txt
HS5,1,60.176715,24.882700,30.0,Waiting for the Boat,boat_waiting.jpeg;boat_waiting.txt
HS6,1,60.176668,24.886685,30.0,Walking the Island,people_walking.jpg;people_walking.txt
HS7,3,60.176625,24.884945,30.0,Hurry Up!,t29.m4a;t29.txt;generic_person.png
HS8,3,60.177376,24.881995,30.0,Waiting Room,t30.m4a;t30.txt;generic_person.png
HS9,3,60.177124,24.882681,30.0,Sten Anders Wallin,t31.m4a;t31.txt;generic_person.png
HS11,1,60.179825,24.880956,30.0,Beach Picnic,bonfires_1915.jpg;bonfires_1915.txt
HS12,1,60.180707,24.881345,30.0,Beach Picnic,bonfires_beach.jpg;bonfires_beach.txt
HS13,1,60.180864,24.882388,30.0,Frithiof Mieritz,restraunt1.jpg;restraunt1.txt
HS14,1,60.179785,24.880951,30.0,The Pier,pier_for_ferry.jpg;pier_for_ferry.txt
HS15,1,60.180980,24.883196,40.0,The Restaurant,woman_heading_to_restraunt.jpg;woman_heading_to_restraunt.txt
HS16,1,60.181265,24.883228,30.0,Restaurant Prices,restaurant_prices.jpeg;restaurant_prices.txt
HS17,3,60.180747,24.882578,30.0,The Restaurant,t25.m4a;t25.txt;generic_person.png
HS18,3,60.180951,24.883074,30.0,The Restaurant,t26.m4a;t26.txt;generic_person.png
HS19,1,60.181813,24.879460,30.0,Swimming,skinny_dippers.jpg;skinny_dippers.txt
HS20,3,60.182358,24.879256,30.0,Swimming,t20.m4a;t20.txt;generic_person.png
HS21,3,60.183715,24.881244,30.0,Go Swimming,t21.m4a;t21.txt;generic_person.png
HS22,1,60.183011,24.885816,30.0,Midsummer Pole,midsummer_pole.jpg;midsummer_pole.txt
HS23,1,60.183184,24.885603,30.0,Folk Dancing,folk_dancing.jpg;folk_dancing.txt
HS24,1,60.182705,24.885631,30.0,Folk Band,folk_music_dancing.jpg;folk_music_dancing.txt
HS25,1,60.183190,24.885583,30.0,Folk Dancing,folkdancing_at_serausaari.jpg;folkdancing_at_serausaari.txt
HS26,1,60.182697,24.886536,30.0,The Church,church_interior.jpg;church_interior.txt
HW18,3,60.182351,24.885283,30.0,The Museum,t11.m4a;t11.txt;generic_person.png
HS30,1,60.180104,24.888101,30.0,Midsummer Bonfires,bonfires.jpg;bonfires.txt
HS31,1,60.178977,24.886843,30.0,Sports,sports_ground.jpg;sports_ground.txt
HS32,3,60.179368,24.885490,30.0,Picking Flowers,t35.m4a;t35.txt;generic_person.png
HS33,3,60.179420,24.884391,30.0,Spells,t36.m4a;t36.txt;generic_person.png
HS35,3,60.176701,24.884641,30.0,Clay Pits,t33.m4a;t33.txt;generic_person.png
HS36,3,60.185723,24.884701,30.0,The People's Park,t3.m4a;t3.txt;generic_person.png
HS37,3,60.185749,24.883731,30.0,Frithiof Mieritz,t4.m4a;t4.txt;generic_person.png
HS38,1,60.185871,24.884227,30.0,Cafe Mieritz,cafe_meritz.jpeg;cafe_meritz.txt
HS39,1,60.185038,24.882444,30.0,Relaxing,midsummer_family.jpg;midsummer_family.txt
HS40,1,60.185038,24.882444,30.0,Playing Cards,playing_cards.jpg;playing_cards.txt
HS41,1,60.176605,24.886925,30.0,People Swimming,people_swimming.jpg;people_swimming.txt
HS42,3,60.182693,24.887909,30.0,The Buildings,t12.m4a;t12.txt;generic_person.png
CW36,3,60.182552,24.887994,30.0,The Open-Air Museum,t10.m4a;t10.txt;generic_person.png
CS2,1,60.1793888889,24.8858472222,30.0,Midsummer Headdress,IMG_5814.JPG;IMG_5814.txt
CS1,1,60.1793027778,24.8858583333,30.0,Festival Grounds,IMG_5816.JPG;IMG_5816.txt
CS3,1,60.179399,24.885271,30.0,Summer Festival Grounds,IMG_5820.JPG;IMG_5814.txt
CS4,1,60.179316,24.885183,30.0,Midsummer Dancing,File_000.jpeg;File_000.txt
CS5,1,60.1792416667,24.8846750000,30.0,Midsummer Dancing,File_001.jpeg;File_001.txt
CS6,3,60.179408,24.885396,30.0,Midsummer,t37.m4a;t37.txt;generic_person.png
CS7,3,60.179237,24.885088,30.0,Midsummer,t39.m4a;t39.txt;generic_person.png
CS8,1,60.178864,24.887197,30.0,Lighting the Bonfire,P1060710.JPG;P1060710.txt
CS9,1,60.179018,24.886768,30.0,Midsummer Bonfires,P1060742.JPG;P1060742.txt
CS10,3,60.179135,24.886657,40.0,Reserving a Place,t38.m4a;t38.txt;generic_person.png
CS11,1,60.179384,24.884590,30.0,The Bridal Couple,bridal_couple.jpeg;bridal_couple.txt
CS12,3,60.179482,24.884032,30.0,See the Bridal Couple,t46.m4a;t46.txt;generic_person.png
CS13,1,60.180348,24.881108,30.0,Fishing on the Dock,IMG_5838.JPG;IMG_5838.txt
CS14,1,60.180264,24.880758,30.0,Geese at the Dock,IMG_5840.JPG;IMG_5840.txt
CS15,3,60.181121,24.881903,30.0,The Restaurant,t27.m4a;t27.txt;generic_person.png
CS16,3,60.180337,24.881090,30.0,The Dock,t28.m4a;t28.txt;generic_person.png
CS17,3,60.182320,24.879451,30.0,The Lido,t22.m4a;t22.txt;generic_person.png
CS18,3,60.182901,24.879670,30.0,Jogging on the Island,t23.m4a;t23.txt;generic_person.png
CS19,1,60.1827925000,24.8858107000,30.0,The Midsummer Pole,P1060636.JPG;P1060636.txt
CS20,1,60.1831118000,24.8859519000,30.0,The Midsummer Pole,P1060637.JPG;P1060637.txt
CS21,1,60.1831638889,24.8854944444,30.0,The Dancing Stage,IMG_2253.JPG;IMG_2253.txt
CS22,2,60.183132,24.885495,30.0,Folkdancing,enkeliska_folkdance.mp4;IMG_2247.txt
CS23,3,60.185552,24.884951,30.0,Seaguls,t6.m4a;t6.txt;generic_person.png
CS24,3,60.185817,24.884118,30.0,Cafe Mieritz,t7.m4a;t7.txt;generic_person.png
CS25,3,60.185808,24.884688,30.0,Driving on the Bridge,t8.m4a;t8.txt;generic_person.png
CS26,3,60.185348,24.885174,30.0,Languages,t9.m4a;t9.txt;generic_person.png
CS27,3,60.182965,24.885921,30.0,The Open Air Museum,t14.m4a;t14.txt;generic_person.png
CS28,3,60.182618,24.886695,30.0,Axel Olai Heikel,t15.m4a;t15.txt;generic_person.png
CS29,3,60.183052,24.885720,30.0,Folk dancing,t19.m4a;t19.txt;generic_person.png
CS30,3,60.182181,24.885029,30.0,The Museum Buildings,t12.m4a;t12.txt;generic_person.png
CS32,1,60.185830,24.881807,30.0,Midsummer Light,P1060788.JPG;P1060788.txt
CS33,1,60.1765777778,24.8844305556,30.0,Ducks,IMG_2133.JPG;IMG_2133.txt
CS34,1,60.1765777778,24.8844305556,30.0,The Nature Ponds,IMG_2155.JPG;IMG_2155.txt
CS35,1,60.1766805556,24.8836250000,30.0,The Nature Ponds,IMG_2120.JPG;IMG_2120.txt
CS36,2,60.176701,24.884641,30.0,Ducks,img_2142.mp4;IMG_2142.txt
CS37,1,60.1765333333,24.8835722222,30.0,Summer View,IMG_2121.JPG;IMG_2121.txt
CS38,2,60.176688,24.885123,30.0,Swimming Birds,img_2166.mp4;IMG_2166.txt
CS39,3,60.176657,24.884554,30.0,The Ponds,t34.m4a;t34.txt;generic_person.png
CS40,3,60.177027,24.882769,30.0,Honey Bees,t32.m4a;t32.txt;generic_person.png
CS41,3,60.177027,24.882769,30.0,Seurasaarisäätiö,t47.m4a;t47.txt;generic_person.png
CS42,2,60.183100,24.885315,30.0,Folk Dancing,ruha_karelian_wedding_dance.mp4;IMG_2247.txt
HW18,3,60.183272,24.885967,30.0,The Museum,t11.m4a;t11.txt;generic_person.png
