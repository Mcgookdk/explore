package davidmcgookin.net.explore;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import davidmcgookin.net.explore.util.TriggeredContent;

public class TriggeredContentActivity extends AppCompatActivity {

    private static final String TAG = "TrigContentActivity";

    //To communicate and Bind with the Service
    private boolean mIsBound = false;
    private ExploreService mExploreService = null;



    private TriggeredContent mTriggeredContent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_triggered_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


EditText et = (EditText)findViewById(R.id.textDisplay);
        //get the tc from the activity
        Intent i = getIntent();
        String s = i.getStringExtra(TriggeredContent.ENCODED_TRIGGERED_CONTENT_STRING);
        Log.v(TAG, s);
        mTriggeredContent = TriggeredContent.parseFromString(s,"",""); //we have no prefix to add to the filenames
        Log.v(TAG, "STRING IS "+mTriggeredContent.toString());




        et.setText(mTriggeredContent.toString());


        Intent intent = new Intent(this, ExploreService.class);
        bindService(intent, mConnection, 0);


    }


protected void onStop(){

    super.onStop();
    if(mIsBound) {
        mExploreService.undipAudio();
        unbindService(mConnection);

    }

}

    /**
     * Defines callbacks for service binding and unbinding events, passed to
     * bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            ExploreService.ExploreServiceBinder binder = (ExploreService.ExploreServiceBinder) service;
            mExploreService = binder.getService();
            mIsBound = true;
            Log.v(TAG, "Service Bound Sucessfully");
            //check if the service is already running.
            mExploreService.triggeredActivityReportsActivation(); //Call the service to say we have been activited and can be cleared.
            mExploreService.dipAudio();
        }

        public void onServiceDisconnected(ComponentName arg0) {
            mIsBound = false;
            Log.v(TAG, "Service Did Not Bind");
        }
    };






}
