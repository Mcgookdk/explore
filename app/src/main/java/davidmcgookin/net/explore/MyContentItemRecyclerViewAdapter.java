package davidmcgookin.net.explore;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import davidmcgookin.net.explore.ContentItemListFragment.OnListFragmentInteractionListener;
import davidmcgookin.net.explore.util.TriggeredContent;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a  and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyContentItemRecyclerViewAdapter extends RecyclerView.Adapter<MyContentItemRecyclerViewAdapter.ViewHolder> {

    private final List<TriggeredContent> mValues;
    private final OnListFragmentInteractionListener mListener;


    public MyContentItemRecyclerViewAdapter(List<TriggeredContent> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_contentitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        TriggeredContent tc = mValues.get(position);
        holder.mtitleView.setText(tc.getId()+" "+tc.getTitle());

        int resID = -1;
        if(tc.getType() == TriggeredContent.VIDEO_TYPE){
           resID = R.drawable.movie_icon;//.drawable.ic_movie_black_24dp;
        }else if(tc.getType() == TriggeredContent.IMAGE_TYPE){
            resID = R.drawable.image_icon;//ic_image_black_24dp;
        }else if (tc.getType() == TriggeredContent.AUDIO_TYPE){
            resID = R.drawable.audio_icon;//ic_volume_up_black_24dp;
        }else{
            // Nothing
        }

        holder.mImageView.setBackgroundColor(Color.TRANSPARENT);
        holder.mImageView.setImageResource(resID);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mtitleView;
       // public final TextView mContentView;
        public final ImageView mImageView;
        public TriggeredContent mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.icon_image_view);
            mtitleView = (TextView) view.findViewById(R.id.title_overview);
           // mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" +  "'";
        }
    }
}
