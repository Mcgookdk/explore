package davidmcgookin.net.explore;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/*
*Activity to requestion permissions from app if needed
* David McGookin
* Version 1.0
* 26 January 2016
 */
public class RequestPermissionsActivity extends AppCompatActivity {


    private static String TAG = "RequestPermissionsActivity";
    AlertDialog cantGetPermissionsDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_permissions);

        Button actionButton = (Button) findViewById(R.id.SetPermissionsButton);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int permissionRequestResult = AndroidApplicationPermissions.requestPermissions(RequestPermissionsActivity.this);
                if (permissionRequestResult == AndroidApplicationPermissions.PERMISSIONS_NEEDED_BUT_ALREADY_DENIED) {
                    Log.v(TAG, "WE CANNOT GET ALL THE PERMISSIONS. NEED TO DIALOG THE USER");
                    if (cantGetPermissionsDialog == null) {
                        createAlertDialog();
                    }
                        cantGetPermissionsDialog.show();

                } else if (permissionRequestResult == AndroidApplicationPermissions.HAVE_ALL_PERMISSIONS) {
                    //Unlikely but possible
                    Log.v(TAG, "HAVE PERMISSIONS, DO SOMETHING");
                    Intent i = new Intent(getApplicationContext(), ExploreMainActivity.class);
                    startActivity(i);
                    setResult(RESULT_OK);
                    finish();


                }
            }
        });
    }


    //This needs to be in the activity
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        if (AndroidApplicationPermissions.processOnRequestPermissionsResult(requestCode, permissions, grantResults)) {
            Log.v(TAG, "HAVE ALL PERMISSIONS");
            Intent i = new Intent(getApplicationContext(), ExploreMainActivity.class);
            startActivity(i);
            setResult(RESULT_OK);
            finish();

        } else {
            Log.v(TAG, "PERMISSIONS WERE DENIED, TELL THE USER");
            if (cantGetPermissionsDialog == null) {
                createAlertDialog();
            }
            cantGetPermissionsDialog.show();
        }

    }

    public void createAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.failed_permission_dialog_text)
                .setTitle(R.string.failed_permission_dialog_title);
        cantGetPermissionsDialog = builder.create();

    }
}
