package davidmcgookin.net.explore;


/**
 * ExploreService
 * David McGookin (davidmcgookin@gmail.com)
 * Created 27th January 2016
 *
 * Acts as the primary service to control the app
 */



//add a timer that starts when current content activated by notification appears onscreen
//it should cancel when the content goes offscreen
//when it is cancelled it should set that there is nothing on screen.
//update the firing logic to take this into account
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import junit.framework.Assert;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import davidmcgookin.net.SimpleLocation.*;
import davidmcgookin.net.explore.TriggeredContentActivitites.AudioContentActivity;
import davidmcgookin.net.explore.TriggeredContentActivitites.ImageContentActivity;
import davidmcgookin.net.explore.TriggeredContentActivitites.MovieContentActivity;
import davidmcgookin.net.explore.util.ApplicationEventLogger;
import davidmcgookin.net.explore.util.ContentAreaMarker;
import davidmcgookin.net.explore.util.EnvironmentalSound;
import davidmcgookin.net.explore.util.GlobalWorkspaceDefines;
import davidmcgookin.net.explore.util.GoogleLocationUtilitites;
import davidmcgookin.net.explore.util.PureDatahelper;
import davidmcgookin.net.explore.util.StepDetectorWalkingSonifier;
import davidmcgookin.net.explore.util.StepSensorWalkingSpeedDetector;
import davidmcgookin.net.explore.util.TriggeredContent;
import davidmcgookin.net.explore.util.UserActivityWalkingSonifier;
import davidmcgookin.net.explore.util.WalkingSonifier;
import davidmcgookin.net.explore.util.WalkingSpeedDetector;
import davidmcgookin.net.explore.util.YearContext;

import static davidmcgookin.net.explore.util.GlobalWorkspaceDefines.*;


public class ExploreService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, StepSensorWalkingSpeedDetector.WalkingSpeedDetectorCallback {




    private YearContext[] mYearList;
    private String[] mSupportedSeasons;
    private int mCurrentSeasonIndex = 0;
    private int mCurrentYearIndex = 0; //DANGER! NEEDS TO BE CHANGED THE FIRST CHANCE WE GET.

    private static final int NUMBER_OF_YEARS = 1;
    private static final int NUMBER_OF_SEASONS= 2;

    private ArrayList<TriggeredContent> mAllTriggeredContents[][];
    private ArrayList<TriggeredContent> mAllPreviouslySeenTriggeredContents[][];
    private ArrayList<EnvironmentalSound> mAllEnvironmentalSounds[][];

    private ArrayList<ContentAreaMarker> mContentAreaMarkers[]; //only one per year!
    //variables to hold content

    //These now are just holding the current version.
    //ArrayList<EnvironmentalSound> mEnvironmentalSounds;
   //ArrayList<TriggeredContent> mTriggeredContents;
    //ArrayList<TriggeredContent> mPreviouslySeenTriggeredContents;


    ArrayList<AudioSourcePlayer> mCurrentlyActiveEnvironmentalSounds;

    //some global defines to help map a season onto someting
    public static final int SUMMER_SEASON = 0;
    public static final int WINTER_SEASON = 1;

    private int notificationID =0; //Used as an integer for the sending local notifications

    public static final String USER_LOCATION_CHANGED = "net.davidmcgookin.explore.ExploreService.USER_LOCATION_CHANGED";
    public static final String LOCATION = "net.davidmcgookin.explore.ExploreService.LOCATION";
    public static final String YEAR_DID_CHANGE = "net.davidmcgookin.explore.ExploreService.YEAR_DID_CHANGE"; //Broadcast if a new year is selected for the app
    public static final String YEAR_STRING = "net.davidmcgookin.explore.ExploreService.YEAR_STRING"; // The current year as a string tag
    public static final String YEAR_INDEX = "net.davidmcgookin.explore.ExploreService.YEAR_INDEX"; // The current year as an index in the array mYearList
    public static final String SEASON_DID_CHANGE = "net.davidmcgookin.explore.ExploreService.SEASON_DID_CHANGE"; //Broadcast if a new season is selected for the app
    public static final String SEASON_INDEX = "net.davidmcgookin.explore.ExploreService.SEASON_INDEX"; // The current season as an index in the array

    public static final String PREVIOUSLY_SEEN_CONTENT_UPDATED = "net.davidmcgookin.explore.ExploreService.PREVIOUS_CONTENT_UPDATED"; // broadcast when the previously seen content is updated. I.e. the user finds something new.

    public static final String NEW_CONTENT_NOTIFICATION_TRIGGERED = "net.davidmcgookin.explore.ExploreService.NEW_CONTENT_NOTIFICATION_TRIGGERED";
    public static final String NEW_CONTENT_NOTIFICATION_CANCELLED = "net.davidmcgookin.explore.ExploreService.NEW_CONTENT_NOTIFICATION_CANCELLED";
public static final String NEW_CONTENT_INTENT = "net.davidmcgookin.explore.ExploreService.NEW_CONTENT_INTENT";


    public static final String BACKGROUND_AUDIO_IS_ACTIVE = "net.davidmcgookin.explore.ExploreService.BACKGROUND_AUDIO_IS_ACTIVE";
    public static final String BACKGROUND_AUDIO_IS_MUTE = "net.davidmcgookin.explore.ExploreService.BACKGROUND_AUDIO_IS_MUTE";
 public static final String BACKGROUND_AUDIO_IS_INACTIVE= "net.davidmcgookin.explore.ExploreService.BACKGROUND_AUDIO_IS_INACTIVE";

    private static final long TIME_DELAY_UNTIL_TRIGGERD_CONTENT_MARKED_AS_INVALID = 60*1000 *1; //one minutes
    public boolean mTriggeredActivityOnscreenIsValid = false;
    private Handler mCancelTriggeredActivityHandler;


    public static final String TRIGERED_CONTENT_LAUNCED_FROM_SERVICE = "net.davidmcgookin.explore.ExploreService.TRIGERED_CONTENT_LAUNCED_FROM_SERVICE";

    //Strings for LogCat and identifying us in the manifest
    public static final String NAME = "explore.davidmcgookin.net.ExploreService.SERVICE";
    public static final String TAG = "ExploreService";
    private final IBinder mBinder = new ExploreServiceBinder();

    //variables for the location and logging of location
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL = 5000;
    // The fastest update frequency, in milliseconds
    private static final long FASTEST_INTERVAL = 5000;
    private Location lastKnowLocation = null;
    private boolean mCurrentlyLogging = false;
   // private boolean mShouldBeLogging = false;
    private boolean mIsServiceActive = false; //true iff we are running the location updates and have loaded content
    //private LocationServiceMonitor mLocationServiceMonitor;
    private boolean mGooglePlayLocationServicesConnected = false; //true iff we are connect to Google Play Location Services.
    private GoogleApiClient mGoogleApiClient;
    //should be checked before we start logging
    private int mLastKnownWalkingSpeedInStepsPerMinute = 0;

    //variables to cover the recording of data locally
    //strings for the logging folder name
    public static final String COMMON_RESULTS_DIRECTORY_NAME = "Explore_Logs";
    private GPXLogger mGPXLogger = null;
    private ApplicationEventLogger mApplicationEventLogger = null;

    //Records if we are currently dipping the audio sound to because of some other activity in the program
    private boolean mDippingAudio = false;

    private boolean mNewTriggeredContentIsOnscreen = false;
    //Variables to deal with the dummy location if we use it
    private final boolean IS_USING_FAKE_LOCATION = true; //set to true if we use the fake location or the GPS
    private LatLng currentFakeUserLocation =  new LatLng(60.186118, 24.884784);  //set to serausaari bridge


    //Variables used for the triggered content
    LatLng mLastUpdatedUserLocation = new LatLng(0.00,0.00); //Set this to something silly so it runs on the first loop
    private static final float MINIMUM_DISTANCE_MOVED_TO_TRIGGER_UPDATE = 3.0f; //the miminum distance the GPS must move from the last updated location to look for new triggered content.

    private  TriggeredContent mCurrentlyActiveTriggeredContent = null;
    private Intent mCurrentNewContentIntent = null;
    private boolean mTriggeredContentReportsActive = false;
    //variables to hold the walking things
  //  WalkingSonifier mWalkingSonifier; CURRENTLY DISABLED. FUNCTIONALITY PROBABLY GOING TO PD

    WalkingSpeedDetector mWalkingSpeedDetector;


    //For audio playback
  //  private AudioSourcePlayer mASP, mASP2;



    //Pure Data Related Items
  //  PureDatahelper pureDataHelper = null;
    @Override
    //return our binder on a bind event
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "CREATED");
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        if (!(googleApiAvailability.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS)) {
            Log.v(TAG, "Google Play Services are Not Available");
        } else {
            Log.v(TAG, "Google Play Services are Available");
        }
        buildGoogleApiClient();
        mGoogleApiClient.connect();

        //Load the years and set an intiial year


        setupYears();
        setupSeasonsForCurrentYear();

        //load the triggered content files and the environmental sounds
        //should only need to do this once!

        setupTriggeredContent();
        setupEnvironmentalSounds();
        setupContentAreaMarkers();
        mCurrentlyActiveEnvironmentalSounds = new ArrayList<AudioSourcePlayer>();

        //setup Pure Data
        // pureDataHelper = new PureDatahelper(this);
     // pureDataHelper.initPd("winter.pd");
     // pureDataHelper.setVolume(1.0f);
   // pureDataHelper.activateSound(PureDatahelper.SEAGUL);
    //pureDataHelper.activateSound(PureDatahelper.SQUIRRELS);
    //   pureDataHelper.activateSound(PureDatahelper.RAIN);
    // pureDataHelper.activateSound(PureDatahelper.WIND);
     //   pureDataHelper.activateStepSonification();

        mAllPreviouslySeenTriggeredContents = new ArrayList[NUMBER_OF_YEARS][NUMBER_OF_SEASONS];
        for(int i = 0; i < NUMBER_OF_YEARS; i++){
            for (int j = 0; j < NUMBER_OF_SEASONS; j++){
                mAllPreviouslySeenTriggeredContents[i][j] = new ArrayList<TriggeredContent>();
            }
        }

      //  mCurrentPreviouslySeenTriggeredContents = new ArrayList<TriggeredContent>();


       // if(GlobalWorkspaceDefines.RUNNING_SELECTABLE_MULTISEASON_VERSION){
         //   this.setSeasonIndex(WINTER_SEASON); //Make winter the default
        //}
    }


    public void muteBackgroundSound(){
        if (mCurrentlyActiveEnvironmentalSounds.size() >=1){
            mCurrentlyActiveEnvironmentalSounds.get(0).mute(true);
            Intent i = new Intent();
            i.setAction(BACKGROUND_AUDIO_IS_MUTE);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
            mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.USER_MUTED_BACKGROUND_AUDIO,"User muted background sound for ID "+mCurrentlyActiveEnvironmentalSounds.get(0).getEnvironmentalSound().getId() +" with Tag "+mCurrentlyActiveEnvironmentalSounds.get(0).getEnvironmentalSound().getTag() );
            Log.v(TAG, "MUTING BACKGROUND SOUND");

        }
    }
    public void onDestroy() {
        Log.v(TAG, "DESTROYED");
        mGoogleApiClient.disconnect();
        super.onDestroy();//last thing we call
    }

    public void walkingSpeedUpdated(int stepsPerSecond){
        //bit complex but we need to handle turing the sound on and off
        if(stepsPerSecond == 0){
            Log.v(TAG,"Deactiviting Step Sonification "+stepsPerSecond);
        //    pureDataHelper.deactivateStepSonification();
        }else if(mLastKnownWalkingSpeedInStepsPerMinute == 0){
        //   pureDataHelper.activateStepSonification();
         //   pureDataHelper.setStepRate(stepsPerSecond);
            Log.v(TAG,"Activiting Step Sonification "+stepsPerSecond);
        }else{
        //    pureDataHelper.setStepRate(stepsPerSecond);
        }
        mLastKnownWalkingSpeedInStepsPerMinute = stepsPerSecond;
    }

 public void startServiceRunning() {

     Assert.assertEquals(mIsServiceActive, false);
    mIsServiceActive = true;

     Notification n = getNotificationForForegroundService();
     this.startForeground(20, n); //run us as a foreground service so we don't get killed!
     this.createLoggingFiles();
     setupEnvironmentalSounds();
     setupTriggeredContent();
     setupBroadcastReceiverForEvents();// this should start receiveing important system broadcasts


     //pureDataHelper.startAudio(); //start the "audio from pd
    // if(mCurrentSeasonIndex == 1){
    //  //   pureDataHelper.setVolume(PureDatahelper.MAX_VOLUME);
    // }else{
    //     pureDataHelper.setVolume(0.01f);
    // }

     if(!IS_USING_FAKE_LOCATION) {
         this.startLocationUpdates();
         Log.v(TAG, "Starting GPS Updates");
     }else{
         Log.v(TAG, "Using Fake Location Provider. Gps Not Started");
         this.setCurrentFakeUserLocation(currentFakeUserLocation); // to seed anything we need
     }


     mWalkingSpeedDetector = new StepSensorWalkingSpeedDetector(this);
     mWalkingSpeedDetector.startWalkingSpeedDetection();
     mWalkingSpeedDetector.setCallback(this);
//THE WALKING SONIFIERS ARE DISABLED FOR THE MOMENT!
     //setup the walking sonifier
   //  if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){ //if we are at KITKAT (4.4) or above there should be a step detector
     //    Log.v(TAG,"Running KitKat so using Step detector");
      //   mWalkingSonifier = new StepDetectorWalkingSonifier(this,"frog.m4a");
     //}else{
       //  Log.v(TAG, "Running Below KitKat so used looped sound based on walking");
        // mWalkingSonifier = new UserActivityWalkingSonifier(this,"Snow.m4a");
     //}
    //mWalkingSonifier.startWalkingSonifier();

     Iterator e = mAllEnvironmentalSounds[mCurrentYearIndex][mCurrentSeasonIndex].iterator();
     while (e.hasNext()){
         Log.v(TAG, "NEW SOUNDS: " + e.next().toString());
     }
    e = mAllTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].iterator();
     while (e.hasNext()){
         Log.v(TAG, "NEW TRIGGERED CONTENT: " + e.next().toString());
     }
    }

    //used by activitites to ensure that we don't try to restart loading if we are already in an "experience"
    public boolean getServiceStatus(){
        return mIsServiceActive;
    }
    public void stopServiceRunning() {
        //doesn't really stop us. Just removes us from the foreground
        Assert.assertEquals(mIsServiceActive, true);
        mIsServiceActive = false;

        //stop pd
        //pureDataHelper.stopAudio();
        //pureDataHelper.cleanupPD();

        //stop the walking sonifier CURRENTLY DISABLED
      //  mWalkingSonifier.stopWalkingSonifier();//

        mWalkingSpeedDetector.stopWalkingSpeedDetection();
        this.stopForeground(true); //remove us from the foreground

        if(!IS_USING_FAKE_LOCATION) {
            this.stopLocationUpdates();
            Log.v(TAG, "Stopping GPS Updates");
        }
        this.closeLoggingFiles();

    }

    //caution do not change this it is a hack. Might also be null!
    public TriggeredContent getCurrentlyActiveTriggeredContent(){
        return mCurrentlyActiveTriggeredContent;
    }

    public ContentAreaMarker[] getContentAreaMarkersForCurrentYear(){
        return mContentAreaMarkers[mCurrentYearIndex].toArray(new ContentAreaMarker[mContentAreaMarkers[mCurrentYearIndex].size()]);
    }
//Returns the triggered content as an array
public TriggeredContent[] getTriggeredContentForCurrentSeason(){

    return mAllTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].toArray(new TriggeredContent[mAllTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].size()]);

}
    //returns the previously seen content
    public TriggeredContent[] getPreviouslySeenTriggeredContent(){
//if(getUsingFakeLocation()){
 //   Log.v(TAG,"USING FAKE LOCATION!!");
  //  return getTriggeredContentForCurrentSeason();
//}

        Object arr[] = mAllPreviouslySeenTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].toArray();//new TriggeredContent[mPreviouslySeenTriggeredContents.size()]);


        TriggeredContent array1[]= Arrays.copyOf(arr, arr.length, TriggeredContent[].class);

         Log.v(TAG,"Array Dump is is: "+arr+ " Array Length is "+arr.length);
         for(int i =0; i < array1.length; i++){
            Log.v(TAG, array1[i].toString());
         }

        return array1;// (TriggeredContent[]) mPreviouslySeenTriggeredContents.toArray();//new TriggeredContent[mPreviouslySeenTriggeredContents.size()]);
    }
   // returns environmental sound as an array

//    public EnvironmentalSound[] getEnvironmentalSound(){
 //       return mAllEnvironmentalSounds[mCurrentYearIndex][mCurrentSeasonIndex].toArray(new EnvironmentalSound[mEnvironmentalSounds.size()]);
//}

    public boolean backgroundAudioIsPlaying(){
        if(mCurrentlyActiveEnvironmentalSounds.size() >= 1){
            return true;
        }
        return false;
    }

    public boolean backgroundAudioIsMuted(){
        if(mCurrentlyActiveEnvironmentalSounds.size() >=1){
            if(!mCurrentlyActiveEnvironmentalSounds.get(0).isMuted()){
                return false;
            }
        }

        Log.v(TAG, "BACKGROUND SOUND MUTED");
        return true;
    }

 public boolean getUsingFakeLocation(){
     return IS_USING_FAKE_LOCATION;
 }


    public void setCurrentFakeUserLocation(LatLng ll){
        currentFakeUserLocation = ll;
        if(IS_USING_FAKE_LOCATION){
            onLocationChanged(GoogleLocationUtilitites.locationFromLatLng(ll, "FAKE LOCATION PROVIDER"));
        }
    }
private void startLocationUpdates() {
        //Start location logging
    Log.v(TAG, "STARTING LOCATION UPDATES");
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Set the update interval to 5 seconds
        locationRequest.setInterval(UPDATE_INTERVAL);
        // Set the fastest update interval to 1 second
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);

    }

    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    public void triggeredActivityReportsActivation(){
        //thismethod also gets called if we are viewing content later so we need to check nothing bad is happening
        //This is a bodge and we should fix this in the content views at some point
        Log.v(TAG, "ACTIVITY REPORTS IT WAS activated");
        if(mCurrentlyActiveTriggeredContent != null) {


            mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.USER_ACTIVATED_NOTIFICATION,"User activated notification for content id: "+mCurrentlyActiveTriggeredContent.getId());

            Intent i = new Intent();
            i.setAction(PREVIOUSLY_SEEN_CONTENT_UPDATED);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);


            mNewTriggeredContentIsOnscreen = true; //there is content onscreen we triggered.
            Log.v(TAG, "CONTENT ONSCREEN");
            //add it to the previously seen list of triggered content

            Log.v(TAG, "Added Triggered Content was " + mCurrentlyActiveTriggeredContent);
            mAllPreviouslySeenTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].add(mCurrentlyActiveTriggeredContent);

            cancelCurrentContentTrigger();


            //Start the timer to lockout a new notification being presented until the user has a chance to read this one
            mNewTriggeredContentIsOnscreen =true; //content is on-screen lockout any new notifications
            mCancelTriggeredActivityHandler = new Handler();
            mCancelTriggeredActivityHandler.postDelayed( new Runnable() {
                @Override
                public void run() {
                    mNewTriggeredContentIsOnscreen = false;
                    Log.v(TAG, "Timer Fired. We have set the Triggered Content to not be onscreen. will now send new notifications");
                }}, TIME_DELAY_UNTIL_TRIGGERD_CONTENT_MARKED_AS_INVALID);

        }

    }


    public void triggeredActivityManuallyGeneratedReportsDeActivation(TriggeredContent tc){
        //only for logging
        Log.v(TAG, "Activity Reports it was Cancelled");
        Log.v(TAG, "CONTENT OFFSCREEN");
        mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.USER_DISMISSED_TRIGGERED_CONTENT_LAUNCHED_FROM_MAP,"User quit triggered content triggered from map (may also be as activity was killed)");

    }
    public void triggeredActivityReportsDeActivation(){
        Log.v(TAG, "Activity Reports it was Cancelled");
        Log.v(TAG, "CONTENT OFFSCREEN");
        mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.USER_DEACTIVATED_NOTIFICATION,"User quit activity from notification");
        mNewTriggeredContentIsOnscreen = false;// there is no content that we triggered onscreen.
       mCancelTriggeredActivityHandler.removeCallbacksAndMessages(null); // cancel the timer as well

    }

    //Location Sensing Relevant Methods
    public void onLocationChanged(Location location) {
        // Called when a new location is found by the network location provider.
        Log.v(TAG, "LOCATION UPDATED: " + location.getProvider() + " " + location.getTime() + " " + location.getLatitude() + " " + location.getLongitude() + " " + location.getAccuracy());
        this.lastKnowLocation = location;
        //We only log a new location when we have a user activity. This avoids sparse data in the CSV file
        mGPXLogger.addTrackpoint(location, "");//log the location and what the user is doing
        //Broadcast the Location Update
        Intent i = new Intent();
        i.setAction(USER_LOCATION_CHANGED);
        i.putExtra(LOCATION, this.lastKnowLocation);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);

        //Check for updates to triggered Content
        updateTriggeredContentForLocationChange(location);
        updateEnvironmentalContentForLocationChange(location);


    }

    public void playPDTest(){ //THIS IS FOR TEST ONLY. SHOULD BE REMOVED!
        //pureDataHelper.playChord(true,1);
        Log.e(TAG, "PLAYED PD TEST");
      //  pureDataHelper.setStepRate(60);
    }

    public Location getLastKnownLocation() {
        Log.v(TAG, "Returning Last Known Location");
        return lastKnowLocation;
    }

//Fired off a notification for a given triggered content
    private void fireNotificationForTriggeredContent(TriggeredContent tc){
        //fire off a notification

        int iconIDSmall;
        int iconIDLarge;
     int notificationSoundFilename =-1;
        long[] tactonPattern = new long[4];
        tactonPattern[0] = 0L; tactonPattern[1] = 200L; tactonPattern[2] = 500L; tactonPattern[3] = 200L;//{ 0L, 200L, 500L, 200L};
        if(GlobalWorkspaceDefines.RUNNING_SELECTABLE_MULTISEASON_VERSION){
            iconIDSmall = R.drawable.star_icon;
            iconIDLarge = R.drawable.star_icon;
            tactonPattern[0] = 0L; tactonPattern[1] = 200L; tactonPattern[2] = 500L; tactonPattern[3] = 200L;
            notificationSoundFilename = R.raw.sample_notification_sound;
        }
        if(GlobalWorkspaceDefines.RUNNING_NONSELECTABLE_MULTISEASON_VERSION){
            if(tc.getSeason() == WINTER_SEASON){
                iconIDSmall = iconIDLarge = R.drawable.ic_winter;
                tactonPattern[0] = 100L; tactonPattern[1] = 100L; tactonPattern[2] = 100L; tactonPattern[3] = 100L;
                notificationSoundFilename = R.raw.winter_notification;
            }else{
                iconIDSmall =iconIDLarge = R.drawable.ic_summer;
                tactonPattern[0] = 200L; tactonPattern[1] = 100L; tactonPattern[2] = 200L; tactonPattern[3] = 100L;
                notificationSoundFilename = R.raw.summer_notification;
            }
        }


        Uri notificationSound = Uri.parse("android.resource://davidmcgookin.net.explore/"+notificationSoundFilename);
        Resources res = getApplicationContext().getResources();
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setContentTitle(res.getString(R.string.user_sample_notification_title_string))
                .setTicker(res.getString(R.string.user_sample_notification_ticker_string))
                .setLights(Color.MAGENTA, 10, 10)
                .setSound(notificationSound)
                .setPriority(Notification.PRIORITY_MAX)
                .setVibrate(tactonPattern)
                .setSmallIcon(iconIDSmall)
                .setLargeIcon( BitmapFactory.decodeResource(null, iconIDLarge))//.drawable.ic_lightbulb) //we also need a small icon!
                .setContentTitle(res.getString(R.string.user_sample_notification_content_title_string))
                .setContentText(res.getString(R.string.user_sample_notification_content_text_string));
        // Creates an explicit intent for an Activity in your app

        Class c;
        if(tc.getType() == TriggeredContent.VIDEO_TYPE){
            c= MovieContentActivity.class;
        }else if(tc.getType() == TriggeredContent.IMAGE_TYPE){
            c = ImageContentActivity.class;
        }else if (tc.getType() == TriggeredContent.AUDIO_TYPE){
            c = AudioContentActivity.class;
        }else{
            c = TriggeredContentActivity.class;
        }
        Intent responseSampleIntent = new Intent(getApplicationContext(), c);
        responseSampleIntent.putExtra(TriggeredContent.ENCODED_TRIGGERED_CONTENT_STRING, TriggeredContent.encodeToString(tc)); //add an encoded string to the triggered content
        responseSampleIntent.putExtra(TRIGERED_CONTENT_LAUNCED_FROM_SERVICE, true);
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        responseSampleIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

    mCurrentNewContentIntent = responseSampleIntent; // save this as we might need it

        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        nm.notify(notificationID, mBuilder.build());

        //Broadcast that the notification has been fired
        Intent i = new Intent();
        i.setAction(NEW_CONTENT_NOTIFICATION_TRIGGERED);
        i.putExtra(NEW_CONTENT_INTENT,responseSampleIntent);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);


    }


    //from a broadcast receiver telling us to activate the content we have.
    public void activateNewContentIntent(){
        mCurrentNewContentIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mCurrentNewContentIntent);
        mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.USER_ACTIVATED_NOTIFICATION_FROM_MAP_VIEW,"User activated notification for content id from Map View : "+mCurrentlyActiveTriggeredContent.getId());
        Log.v(TAG, "Starting Intent from explicit call");

    }

    public void userDismissedNewContent(){
        cancelCurrentContentTrigger();
    }
private void cancelCurrentContentTrigger(){
    NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
    //we are called by the content activity. However, we might nto have started it so make sure this needs cleared.
    if(mCurrentlyActiveTriggeredContent != null) {
        nm.cancel(notificationID);
        notificationID++; // So we set it to something new next time
        mCurrentlyActiveTriggeredContent = null;
        //Broadcast that the notification has been fired
        Intent i = new Intent();
        i.setAction(NEW_CONTENT_NOTIFICATION_CANCELLED);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
        mCurrentNewContentIntent = null;
    }
}


    private void updateTriggeredContentForLocationChange(Location newLocation){


        if(newLocation.distanceTo(GoogleLocationUtilitites.locationFromLatLng(mLastUpdatedUserLocation, "")) > MINIMUM_DISTANCE_MOVED_TO_TRIGGER_UPDATE) {

            //record a new last updated position
            mLastUpdatedUserLocation = GoogleLocationUtilitites.latLngFromLocation(newLocation);


            //Do we have a pending notification of a content to display?
            //If so is it still valid?
            if (mCurrentlyActiveTriggeredContent != null) {
                if (newLocation.distanceTo(GoogleLocationUtilitites.locationFromLatLng(mCurrentlyActiveTriggeredContent.getLocation(), "UTILITY")) > mCurrentlyActiveTriggeredContent.getDeactivationRadius()) {
                    mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.NOTIFICATION_WAS_AUTODISMISSED,"Content ID: "+mCurrentlyActiveTriggeredContent.getId()+" was autodismissed");
                    cancelCurrentContentTrigger();
                }
            }
            //If the content we had is no longer valid, or we didn't have any
            if (mCurrentlyActiveTriggeredContent == null ){

                TriggeredContent candidateTriggeredContent = null;

                for (int i = 0; i < mAllTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].size(); i++) {
                    TriggeredContent tc = mAllTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].get(i);
                    if ((newLocation.distanceTo(GoogleLocationUtilitites.locationFromLatLng(tc.getLocation(), "")) < tc.getActivationRadius()) &&
                    (!hasBeenPreviouslySeen(tc))) {
                        //we want to find the closest
                        if(candidateTriggeredContent == null){
                            candidateTriggeredContent = tc;
                        }else if ((newLocation.distanceTo(GoogleLocationUtilitites.locationFromLatLng(tc.getLocation(), ""))) <
                                (newLocation.distanceTo(GoogleLocationUtilitites.locationFromLatLng(candidateTriggeredContent.getLocation(), "")))){
                                candidateTriggeredContent = tc;
                        }

                    }
                }


                //We should trigger this tc object
                if(!mNewTriggeredContentIsOnscreen && candidateTriggeredContent !=null) { // we only do this if there is not current content onscreen, which has been left onscreen fro less that a timeout
                    //this is a kidof inefficient way to do this, but we want to know for logging purposes
                    mCurrentlyActiveTriggeredContent = candidateTriggeredContent;
                    fireNotificationForTriggeredContent(mCurrentlyActiveTriggeredContent);
                    mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.NOTIFICATION_WAS_FIRED,"Notification triggered for Content ID: "+mCurrentlyActiveTriggeredContent.getId());
                    //Intent dialogIntent = new Intent(this, TriggeredContentActivity.class);
                    //dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //startActivity(dialogIntent);
                     //This does have a race condition that we might not find some content if the activation radius overlaps
                }else if(mNewTriggeredContentIsOnscreen && candidateTriggeredContent !=null){
                    mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.NOTIFICATION_WAS_FOUND_BUT_NOT_FIRED,"New Content was found, but not fired as content is still onscreen");
                    //else the content has been onscreen for less than the time so we log that we could havebut didn't present it
                   // Log.v(TAG, "Content found but not triggered because of timeout");
                }

            }


        }

        }


    //a pass through of the event logger for any activites we activte to use
    public void logEvent(long tSinceEpoc, int code, String description){
        if(mApplicationEventLogger != null) {
            mApplicationEventLogger.logEvent(tSinceEpoc, code, description);
        }
    }

    //returns true iff the given tc is in the list of previouslt seen content
    private boolean hasBeenPreviouslySeen(TriggeredContent tc){
        Iterator<TriggeredContent> tci = mAllPreviouslySeenTriggeredContents[mCurrentYearIndex][mCurrentSeasonIndex].iterator();
        while(tci.hasNext()){
            if(tci.next().isEqual(tc)){
                return true;
            }
        }
        return false;
    }

    private void updateEnvironmentalContentForLocationChange(Location newLocation){

        //create a copy of the environmental sound list
        ArrayList<EnvironmentalSound> tempEnvironmentalSounds = (ArrayList<EnvironmentalSound>)(mAllEnvironmentalSounds[mCurrentYearIndex][mCurrentSeasonIndex]).clone(); //creates a shallow copy of the array list

        //go through the existing audio players and update
        for(int i = 0; i < mCurrentlyActiveEnvironmentalSounds.size(); i++){

            AudioSourcePlayer asp = mCurrentlyActiveEnvironmentalSounds.get(i);
           if(!asp.updateVolume(newLocation)){
               //The sound is outside the deactivation zone, and should be stopped and removed
               asp.stop();
               mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.BACKGROUND_SOUND_STOPPED,"Background Sound stopped with ID: "+asp.getEnvironmentalSound().getId() +" with Tag "+asp.getEnvironmentalSound().getTag());
               mCurrentlyActiveEnvironmentalSounds.remove(i);
               Intent it = new Intent();
               it.setAction(BACKGROUND_AUDIO_IS_INACTIVE);
               LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(it);
           }

            //since this asp has been updated we don't need to consider it later so can remove it from our temporary list
            tempEnvironmentalSounds.remove(asp.getEnvironmentalSound());

        }


        //Add in any new sounds that were created
        for(int i = 0; i < tempEnvironmentalSounds.size(); i++){
            EnvironmentalSound es = (EnvironmentalSound) tempEnvironmentalSounds.get(i);

            if(newLocation.distanceTo(GoogleLocationUtilitites.locationFromLatLng(es.getLocation(),"")) < es.getActivationRadius()){
                Intent it = new Intent();
                it.setAction(BACKGROUND_AUDIO_IS_ACTIVE);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(it);
                AudioSourcePlayer asp = new AudioSourcePlayer(es);
                asp.setUpMediaPlayer(this);
                asp.setLoops(true); //we always want to loop the sound
                if(mDippingAudio){ //check if we are currently dipping sound so that when we plap
                    asp.dipSound();
                }
                asp.updateVolume(newLocation);
                asp.play();
                mCurrentlyActiveEnvironmentalSounds.add(asp);
                mApplicationEventLogger.logEvent(System.currentTimeMillis(),ApplicationEventLogger.NEW_BACKGROUND_SOUND_STARTED,"Background Sound started with ID: "+es.getId() +" with Tag "+es.getTag());
            }
        }

    }



    private void flushAndClearCurrentContent(){
        //clear all the environmental content we have
        for(int i =0; i < mCurrentlyActiveEnvironmentalSounds.size(); i++){
            AudioSourcePlayer asp = mCurrentlyActiveEnvironmentalSounds.get(i);
            asp.stop();
        }



        //null everything else out and let the gc remove them
        mCurrentlyActiveEnvironmentalSounds = null;
      //  mEnvironmentalSounds = null;
       // mTriggeredContents = null;

            //make this the last thing. Possible race condition
        if(mCurrentlyActiveTriggeredContent!= null){
            cancelCurrentContentTrigger();
        }

        //do we have a timer for onscreen content. Then cancel it
        if(mCancelTriggeredActivityHandler != null){
            mCancelTriggeredActivityHandler.removeCallbacks(null);
            mNewTriggeredContentIsOnscreen = false;
        }

    }



    //called when the season is updated
    private void updateForSeasonChange(){

        //do we have a handler.

        //Updates the content in response to a new season

        flushAndClearCurrentContent();
        mCurrentlyActiveEnvironmentalSounds = new ArrayList<AudioSourcePlayer>(5);


        //Check for updates to triggered Content

        updateTriggeredContentForLocationChange(getLastKnownLocation());
        updateEnvironmentalContentForLocationChange(getLastKnownLocation());

        if(mCurrentSeasonIndex == 1){
          //  pureDataHelper.stopAudio();
           // pureDataHelper.initPd("winter.pd");
          //  pureDataHelper.setVolume(PureDatahelper.MAX_VOLUME);
        }else{
         //   pureDataHelper.stopAudio();
          //  pureDataHelper.initPd("summer.pd");
           // pureDataHelper.setVolume(PureDatahelper.MIN_VOLUME);
        }

    }

    //called when the year changes to setup the new content
    private void updateForYearChange(){

        setupSeasonsForCurrentYear();

        //if our current season doesn't exist in the new year (not sure if this is remotely common) then set it back to default.
        if(mCurrentSeasonIndex >= mSupportedSeasons.length){
            mCurrentSeasonIndex = 0;
        }
        updateForSeasonChange();
    }

    //Sets up the notification we need for a permananet service running
    private Notification getNotificationForForegroundService() {
        Resources res = getApplicationContext().getResources();
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());
        mBuilder.setContentTitle(this.getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText("Service is Running OK");
        // Creates an explicit intent for an to take the user to the main activity
        Intent mainActivityIntent = new Intent(getApplicationContext(), LaunchActivity.class);

        // The stack builder object will contain an artificial back stack for the started Activity.
        // This ensures that navigating backward from the Activity leads
        // out of the application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        // Adds the back stack for the Intent (but not the Intent
        // itself)
     //   stackBuilder.addParentStack(LauncherActivity.class);
        // Adds the Intent that starts the Activity to the top of the
        // stack
        stackBuilder.addNextIntent(mainActivityIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        return mBuilder.build(); //return the notification

    }


  public LatLng getDefaultLocation(){
        return new LatLng(60.180973,24.884579); //at the moment this takes us to seraussari
    }




    //Methods to query the service

    //returns a list of year tags representing each historic period we have content
   public String[] getYearTags(){
        String[] sArr = new String[mYearList.length];
        for (int i =0; i < mYearList.length; i++){
                sArr[i] = mYearList[i].getYear();
        }
     return sArr;
    }


    public String []getSeasonTags(){
        Resources res = getResources();

        String[] arr = {res.getString(R.string.summer_title_string), res.getString(R.string.winter_title_string)};
        return arr;
    }

    public int getCurrentSeasonIndex(){
        return mCurrentSeasonIndex;
    }
    //sets the current temporal period to be the given string. s should be one string returned from getYearTags
   public  void setYearIndex(int i){
            mCurrentYearIndex = i;
       updateForYearChange();
        //Broadcast the change of year
        Intent intent = new Intent();
        intent.setAction(YEAR_DID_CHANGE);
        intent.putExtra(YEAR_STRING, mYearList[mCurrentYearIndex].getYear());
        intent.putExtra(YEAR_INDEX, mCurrentYearIndex);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

    }
    public String getYearTag(){
     return  mYearList[mCurrentYearIndex].getYear();
    }

    //sets the current temporal period to be the given string. s should be one string returned from getYearTags
    public void setSeasonIndex(int i){
        mCurrentSeasonIndex = i; updateForSeasonChange();
        //Broadcast the change of year
        Intent intent = new Intent();
        intent.setAction(SEASON_DID_CHANGE);
        intent.putExtra(SEASON_INDEX, mCurrentSeasonIndex);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);


    }

        //Supports logging of events from other parts of the application. Courtesy method only. Don't use for events that are handled by the service and
        //which the activity  is only passing on to the service

     public void logApplicationEvent(long timeSinceEpoc, int code, String description){
        if(mCurrentlyLogging){
                mApplicationEventLogger.logEvent(timeSinceEpoc, code, description);
        }
    }



public void dipAudio(){
    mDippingAudio = true;
    for(int i =0; i < mCurrentlyActiveEnvironmentalSounds.size(); i++){
        AudioSourcePlayer asp = mCurrentlyActiveEnvironmentalSounds.get(i);
        asp.dipSound();
    }

}

    public void undipAudio(){
        mDippingAudio = false;
        for(int i =0; i < mCurrentlyActiveEnvironmentalSounds.size(); i++){
            AudioSourcePlayer asp = mCurrentlyActiveEnvironmentalSounds.get(i);
            asp.undipSound();
        }
    }



    private void setupContentAreaMarkers(){
        mContentAreaMarkers = new ArrayList[NUMBER_OF_YEARS];
        for(int i =0; i < NUMBER_OF_YEARS; i++) {
            mContentAreaMarkers[i] = new ArrayList<ContentAreaMarker>();
            //Open the file
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(
                        new InputStreamReader(getAssets().open(getFilePrefixForYear(i) + "content_areas.txt")));

                // do reading, usually loop until end of file reading
                String line;
                while ((line = reader.readLine()) != null) {
                    Log.v(TAG, "Parsing new of content areas " + line);
                   ContentAreaMarker cam = ContentAreaMarker.parseFromString(line);
                    mContentAreaMarkers[i].add(cam);

                }
            } catch (IOException e) {
                Log.v(TAG, "IO Exception reading the cams");
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Log.v(TAG, "IO Exception reading the cams and trying to close the file");
                    }
                }
            }

        }
    }





    //
    // Files to Setup the content
    // Note these will use the prefix string for the current year and season
    //

    private void setupTriggeredContent() {

        mAllTriggeredContents = new ArrayList[NUMBER_OF_YEARS][NUMBER_OF_SEASONS];

        for (int i = 0; i < NUMBER_OF_YEARS; i++) {
            for (int j = 0; j < NUMBER_OF_SEASONS; j++) {

                mAllTriggeredContents[i][j] = new ArrayList<TriggeredContent>(15);


                //determine locale
                //get the locale string. Need to do this for all languages we support
                String languagePrefixString = "";
                Locale l = Locale.getDefault();
                Log.v(TAG, "locale is " + l + "Display Language" + l.getDisplayLanguage());
                if (l.getDisplayLanguage().equalsIgnoreCase("suomi")) {
                    languagePrefixString = "fi-";
                }

                //setup the namemap. This must be called first and recalled on each setup
                //it feels a bit like a hack!
                TriggeredContent.setupIDNameMap(this, getFilePrefixForYearAndSeason(i, j), languagePrefixString);


                //Open the file
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(
                            new InputStreamReader(getAssets().open(getFilePrefixForYearAndSeason(i, j) + "triggered_content.txt")));

                    // do reading, usually loop until end of file reading
                    String line;
                    while ((line = reader.readLine()) != null) {
                        Log.v(TAG, "Parsing new triggered content" + line);
                        TriggeredContent tc = TriggeredContent.parseFromString(line, getFilePrefixForYearAndSeason(i, j) + "content/", languagePrefixString);
                        tc.setSeason(j); //set the season code for this triggered content, it might be needed
                        mAllTriggeredContents[i][j].add(tc);

                    }
                } catch (IOException e) {
                    Log.v(TAG, "IO Exception reading the triggered content");
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            Log.v(TAG, "IO Exception reading the triggered content and trying to close the file");
                        }
                    }
                }

            }
        }


        //If we are running the single version. Copy everything into the one array!
        if (GlobalWorkspaceDefines.RUNNING_NONSELECTABLE_MULTISEASON_VERSION) {
            Log.v(TAG, "COPYING ALL 1800 content into the first array index");
            for (int i = 0; i < mAllTriggeredContents[0][1].size(); i++) {
                mAllTriggeredContents[0][0].add(mAllTriggeredContents[0][1].get(i));
            }

        }
    }

   // public String seasonStringFromIndexCode(String s){
    //    if(i == "0"){
      //      return "SUMMER";
       // }else if(i==1){
        //    return "WINTER";
        //}else{
         //   return "ERROR ILLEGAL SEASON";
    //    }
    //}


    //loads teh seasons for the currently active year.
    //Note should not be called before setupYears

    private void setupSeasonsForCurrentYear(){
        ArrayList<String> seasons = new ArrayList<String>(2);

        //Open the file
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open(mYearList[mCurrentYearIndex].getYear()+"/seasons.txt")));

            // do reading, usually loop until end of file reading
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                seasons.add(line);

            }
        } catch (IOException e) {
            Log.v(TAG, "IO Exception loading the Seasons for Year:"+mYearList[mCurrentYearIndex].getYear());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.v(TAG, "IO Exception loading the Seasons for year and trying to close the file");
                }
            }
        }

        mSupportedSeasons = seasons.toArray(new String[seasons.size()]); //put them in an array
    }

    private void setupYears(){
        ArrayList<YearContext> years = new ArrayList<YearContext>(2);

        //Open the file
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("years.txt")));

            // do reading, usually loop until end of file reading
            String line;
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                years.add(YearContext.parseYear(line));
              //  years.add(line);

            }
        } catch (IOException e) {
            Log.v(TAG, "IO Exception loading the years"+e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.v(TAG, "IO Exception loading the years and trying to close the file");
                }
            }
        }

        mYearList = years.toArray(new YearContext[years.size()]); //put them in an array
    }



    private void setupBroadcastReceiverForEvents(){
        BroadcastReceiver bcr= new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                // TODO Auto-generated method stub
                Log.v(TAG, "Broadcast reciever called");

                if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                    Log.v(TAG, "SCREEN ON");
                  logEvent(System.currentTimeMillis(), ApplicationEventLogger.SCREEN_ON,"The device screen came on");


                }
                else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    Log.v(TAG, "SCREEN OFF");
                    logEvent(System.currentTimeMillis(), ApplicationEventLogger.SCREEN_OFF,"The device screen went off");

                }
                else if (intent.getAction().equals(Intent.ACTION_BATTERY_LOW)) {
                    Log.v(TAG, "low bat");
                    logEvent(System.currentTimeMillis(), ApplicationEventLogger.LOW_BATTERY,"The device battery is low!!");

                }
                else if (intent.getAction().equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
                    Log.v(TAG, "airplane mode");
                    logEvent(System.currentTimeMillis(), ApplicationEventLogger.OTHER_UNUSUAL_CONDITION,"Airplane mode changed");

                }
                else if (intent.getAction().equals(Intent.ACTION_CALL)) {
                    Log.v(TAG, "user is calling");
                   logEvent(System.currentTimeMillis(), ApplicationEventLogger.OTHER_UNUSUAL_CONDITION,"User is calling");

                }
            }
        };

        registerReceiver(bcr, new IntentFilter(Intent.ACTION_SCREEN_ON));
        registerReceiver(bcr, new IntentFilter(Intent.ACTION_SCREEN_OFF));
        registerReceiver(bcr, new IntentFilter(Intent.ACTION_BATTERY_LOW));
        registerReceiver(bcr, new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED));
        registerReceiver(bcr, new IntentFilter(Intent.ACTION_CALL));


    }



    private void setupEnvironmentalSounds(){


        mAllEnvironmentalSounds = new ArrayList[NUMBER_OF_YEARS][NUMBER_OF_SEASONS];

        for(int i =0; i < NUMBER_OF_YEARS; i++){
            for(int j = 0; j < NUMBER_OF_SEASONS; j++) {


                mAllEnvironmentalSounds[i][j] = new ArrayList<EnvironmentalSound>(15);

                //Open the file
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(
                            new InputStreamReader(getAssets().open(getFilePrefixForYearAndSeason(i,j) + "environmental_sounds.txt")));

                    // do reading, usually loop until end of file reading
                    String line;
                    while ((line = reader.readLine()) != null) {
                        EnvironmentalSound es = EnvironmentalSound.parseFromString(line, getFilePrefixForYearAndSeason(i,j) + "content/");
                        mAllEnvironmentalSounds[i][j].add(es);

                    }
                } catch (IOException e) {
                    Log.v(TAG, "IO Exception reading the environmental sounds" + e);
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            Log.v(TAG, "IO Exception reading the environmental sounds and trying to close the file");
                        }
                    }
                }




            }
        }
    }
    //Setup Logging Files
    private void createLoggingFiles() {

        //Create a common filename part for all logging files
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss"); //UTC format
        String timeStr = sdf.format(new GregorianCalendar().getTime());

        TelephonyManager tMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        Log.v(TAG,"PHONE NUMBER IS "+mPhoneNumber);
        String commonLogFilenamePart = timeStr+"-"+mPhoneNumber;//add the phone number as a uuid


        try{
            //Create a Common directory or check it exists
            File basePath = Environment.getExternalStorageDirectory(); //Android 2.1 is pretty old way of doing this/ Might want to update at some point
            File directory = new File(basePath, ExploreService.COMMON_RESULTS_DIRECTORY_NAME);

            if (!directory.exists()) { //make the directory if we need to
                directory.mkdir();
            }

            //Create a GPX Logger file
            basePath = Environment.getExternalStorageDirectory(); //Android 2.1 is pretty old way of doing this/ Might want to update at some point
            directory = new File(basePath, ExploreService.COMMON_RESULTS_DIRECTORY_NAME + "/" + "GPX_LOGS/");

            if (!directory.exists()) { //make the directory if we need to
                directory.mkdir();
            }

            File filename = new File(directory, "gpxLog" + commonLogFilenamePart + ".gpx");
            mGPXLogger = new GPXLogger(filename);


            //Create and Application Event Log
            basePath = Environment.getExternalStorageDirectory(); //Android 2.1 is pretty old way of doing this/ Might want to update at some point
            directory = new File(basePath, ExploreService.COMMON_RESULTS_DIRECTORY_NAME + "/" + "APPLICATION_EVENT_LOGS/");

            if (!directory.exists()) { //make the directory if we need to
                directory.mkdir();
            }

            filename = new File(directory, "appEventLog" + commonLogFilenamePart + ".csv");
            mApplicationEventLogger = new ApplicationEventLogger(filename);



            mCurrentlyLogging = true;


            //log global things
            if(Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("suomi")){
                logEvent(System.currentTimeMillis(), ApplicationEventLogger.APP_LANGUAGE,"App runs in Finnish");
            }else{
                logEvent(System.currentTimeMillis(), ApplicationEventLogger.APP_LANGUAGE,"App runs in English");

            }
            logEvent(System.currentTimeMillis(), ApplicationEventLogger.PHONE_NUMBER,"Phone is: "+mPhoneNumber);


            if(GlobalWorkspaceDefines.RUNNING_NONSELECTABLE_MULTISEASON_VERSION){
                logEvent(System.currentTimeMillis(), ApplicationEventLogger.APP_VERSION,"RUNNING SINGLE SEASON VERSION");

            }else{
                logEvent(System.currentTimeMillis(), ApplicationEventLogger.APP_VERSION,"RUNNING MULTI SWITCHABLE SEASON VERSION");

            }

        }catch(IOException e){
            e.printStackTrace();
            Toast.makeText(this, "Couldn't create file", Toast.LENGTH_LONG).show();
            Log.v(TAG, "Couldn't create a file");
        }

        Log.v(TAG,"Created Logging Files OK");
    }

    private void closeLoggingFiles(){
        mGPXLogger.closeLog();
        mApplicationEventLogger.logEvent(System.currentTimeMillis(), ApplicationEventLogger.LOG_FILE_CLOSED,"");
        mApplicationEventLogger.closeLog();

        //null the files
        mGPXLogger = null;
        mApplicationEventLogger = null;

        mCurrentlyLogging = false;


        //crash the app
        System.exit(-1);
        Log.v(TAG, "CRASHING");
    }

    //For Google Play Services
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(ActivityRecognition.API)
                .build();
    }

    public void onConnected(Bundle connectionHint) {
        //we are connected to Google Play Services
        mGooglePlayLocationServicesConnected = true;
        Log.v(TAG,"Connected to Play Services");
        ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates(
                mGoogleApiClient,
                1000,
                getActivityDetectionPendingIntent()
        );//.setResultCallback(this);
    }
    private PendingIntent getActivityDetectionPendingIntent() {
        Intent intent = new Intent(this, ActivityRecognitionService.class);

        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // requestActivityUpdates() and removeActivityUpdates().
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    public void onConnectionSuspended (int cause){
        //Called if there is a temporatry failure to connect to play serices
        //onConnected Should be called later. Not sure how we handle this at the moment. But we probably should.
        mGooglePlayLocationServicesConnected = false;
        Log.v(TAG, "Google Player Services Connection Suspended");
    }

    public void onConnectionFailed(ConnectionResult result){
        //The connection to Google play services failed
        mGooglePlayLocationServicesConnected = false;
        Log.v(TAG,"Connection to Google play Services Failed");
    }


//returns the commonprefix for files given the seasons and year we are currently in
private String getFilePrefixForCurrentYearAndSeason(){
    return getFilePrefixForYearAndSeason(mCurrentYearIndex,mCurrentSeasonIndex);

   // return mYearList[mCurrentYearIndex].getYear()+"/"+mSupportedSeasons[mCurrentSeasonIndex]+"/";
}

    private String getFilePrefixForYear(int yearID){
        return mYearList[yearID].getYear()+"/";
    }
    private String getFilePrefixForYearAndSeason(int yearID, int seasonID){
        return getFilePrefixForYear(yearID)+mSupportedSeasons[seasonID]+"/";
    }

public String getFontnameForCurrentYear(){
    return mYearList[mCurrentYearIndex].getYearFont();
}
    public int getColorForCurrentYear(){
        return mYearList[mCurrentYearIndex].getYearColor();
    }



    //Internal class to implement our binding, so we can pass back when a service wants to know something.
    public class ExploreServiceBinder extends Binder {
       public ExploreService getService() {
            // Return this instance of LocalService so clients can call public methods
            return  ExploreService.this;
        }





        public void onConnectionFailed(Bundle arg0) {
            Log.v(TAG, "Could not connect to Google Play Services for location updates");


        }


        public void onConnected(Bundle arg0) { //Once we are connected to location updates, we can start requesting them
            Log.v(TAG, "Connected to Google Play Services for location updates");

        }



        public void onDisconnected() {
            Log.v(TAG, "Dis-Connected from Google Play Services for location updates");


        }}}
