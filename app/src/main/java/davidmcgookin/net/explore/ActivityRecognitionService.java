package davidmcgookin.net.explore;

import android.app.IntentService;
import android.content.Intent;
import android.content.IntentSender;
import android.provider.SyncStateContract;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;

/**
 * Created by davidmcgookin on 16/02/16.
 */
public class ActivityRecognitionService extends IntentService {

    private static final String TAG = "ActivityRecService";

    public static final String ACTIVITY_RECOGNITION_BROADCAST_ACTION = "davidmcgookin.net.explore.ActivityRecognitionService.BROADCAST_ACTION";
    public static final String ACTIVITY_RECOGNITION_PAYLOAD = "davidmcgookin.net.explore.ActivityRecognitionService.ACTIVITY_RECOGNITION_PAYLOAD";
    public static final String ACTIVITY_IN_VEHICLE = "In Car";
    public static final String ACTIVITY_ON_BICYCLE = "On Bicycle";
    public static final String ACTIVITY_ON_FOOT = "On Foot";
    public static final String ACTIVITY_RUNNING = "Running";
    public static final String ACTIVITY_STILL = "Still";
    public static final String ACTIVITY_TILTING = "Tilting";
    public static final String ACTIVITY_WALKING = "Walking";
    public static final String ACTIVITY_UNKNOWN = "Unknown Activity";
    public static final String ACTIVITY_UNIDENTIFIABLE = "Unidentifiable Activity";

    public ActivityRecognitionService() {
        super(TAG); //use the tag to name the worker thread we spin off
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
        Intent localIntent = new Intent(ACTIVITY_RECOGNITION_BROADCAST_ACTION);

        // Get the list of the probable activities associated with the current state of the
        // device. Each activity is associated with a confidence level, which is an int between
        // 0 and 100.
        ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();

        // Log each activity.
        Log.v(TAG, "activities detected");
        for (DetectedActivity da : detectedActivities) {
            Log.v(TAG, getActivityString(da.getType()) + " " + da.getConfidence() + "%");
        }

        // Broadcast the list of detected activities.
        localIntent.putExtra(ACTIVITY_RECOGNITION_PAYLOAD, detectedActivities);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);



    }


    public String getActivityString(int detectedActivityType) {

        switch (detectedActivityType) {
            case DetectedActivity.IN_VEHICLE:
                return ACTIVITY_IN_VEHICLE;
            case DetectedActivity.ON_BICYCLE:
                return ACTIVITY_ON_BICYCLE;
            case DetectedActivity.ON_FOOT:
                return ACTIVITY_ON_FOOT;
            case DetectedActivity.RUNNING:
                return ACTIVITY_RUNNING;
            case DetectedActivity.STILL:
                return ACTIVITY_STILL;
            case DetectedActivity.TILTING:
                return ACTIVITY_TILTING;
            case DetectedActivity.UNKNOWN:
                return ACTIVITY_UNKNOWN;
            case DetectedActivity.WALKING:
                return ACTIVITY_WALKING;
            default:
                return ACTIVITY_UNIDENTIFIABLE;
        }


    }
}
