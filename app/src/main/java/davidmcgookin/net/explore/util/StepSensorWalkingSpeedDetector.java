package davidmcgookin.net.explore.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import davidmcgookin.net.explore.util.WalkingSpeedDetector;

/**
 * Created by davidmcgookin on 20/05/16.
 */
public class StepSensorWalkingSpeedDetector implements WalkingSpeedDetector, SensorEventListener {



    private static final int STEP_RATE_CALLBACK_DELTA_TOLERANCE = 10;
    private static final int ONE_MIN_IN_MS = 60000;
    private static final String TAG ="StepSensWalkSpeedDet";

    private WalkingSpeedDetectorCallback mCallback = null;

    private List<Long> mListOfStepTimestamps = null;
    private Context mContext;
    private SensorManager mSensorManager;
    private Sensor mSensor;

    private Timer mCallbackTimer = null;


    private int mLastCalculatedStepsPerMinute= 0;
    private int mCallbackTolerance = STEP_RATE_CALLBACK_DELTA_TOLERANCE;

    public  StepSensorWalkingSpeedDetector(Context c){
        mContext = c;
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        for (int i = 0; i < deviceSensors.size(); i++) {
            Sensor s = deviceSensors.get(i);
            Log.v(TAG, s.toString());

        }

        mListOfStepTimestamps = new ArrayList<>();
        mCallbackTimer = new Timer();

    }


    public void startWalkingSpeedDetection() {

        if (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
            Log.v(TAG, "HAVE STEP DETECTOR");
        } else {
            Log.v(TAG, "NO STEP DETECTOR AVAILABLE");
        }
    }

        public void stopWalkingSpeedDetection(){
            mSensorManager.unregisterListener(this);
        }


    public void setWalkingSpeedTolerance(int stepsPerSecond){
        mCallbackTolerance = stepsPerSecond;
    }


    private void resetCallbackTimer(){
        mCallbackTimer.purge();
        mCallbackTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                //Update the step sensor
                StepSensorWalkingSpeedDetector.this.timerCallbackFromStepUpdate();
            }
        }, ONE_MIN_IN_MS);
    }


    private void timerCallbackFromStepUpdate() {
        Log.v(TAG, "TIME CALLBACK FOR STEP UPDATE");
        updateStepCountRate();
        resetCallbackTimer();
    }
    public void onSensorChanged (SensorEvent e){
     //   Log.v(TAG, "Step Happened" + e.toString());
        mListOfStepTimestamps.add(System.currentTimeMillis());


        //check and update the step count
        updateStepCountRate();

        //Set a timer to callback in 1 minute if we have no readings.
        resetCallbackTimer();


    }

    public void onAccuracyChanged(Sensor sensor, int accuracy){
        Log.v(TAG, "ACCURACY CHANGED"+accuracy);
    }


    public void setCallback(WalkingSpeedDetectorCallback cb){
        mCallback = cb;
    }

    public void removeCallback(){
        mCallback = null;
    }

    private void updateStepCountRate(){


        //might need a recurrent time to call this method. If it only gets called when the user takes a step, that might never detect
        //Find the last minute of time and count the steps
        long minUseTime = System.currentTimeMillis() - ONE_MIN_IN_MS; // we are only interested in timestamps after this

        int stepsTaken = 0;

        for(int i = 0; i < mListOfStepTimestamps.size(); i++) {

            if (mListOfStepTimestamps.get(i).longValue() > minUseTime) { //We should count it
                stepsTaken++;
            } else {
                mListOfStepTimestamps.remove(i); //We don't need this entry anymore so remove it
            }

        }


        //Check if this is a significant change
        if (Math.abs(stepsTaken - mLastCalculatedStepsPerMinute) > mCallbackTolerance){
            Log.v(TAG, "STEP RATE CHANGED FIRING CALLBACK " + stepsTaken);
            mLastCalculatedStepsPerMinute = stepsTaken;
            if(mCallback != null){
                mCallback.walkingSpeedUpdated(mLastCalculatedStepsPerMinute);
            }
        }else{
       //     Log.v(TAG, "STEP RATE CHANGED BUT NOT ENOUGH TO FIRE CALLBACK");
        }
    }


}
