package davidmcgookin.net.explore.util;

import android.util.Log;

/**
 * Created by davidmcgookin on 11/02/16.
 */
public class VideoPayload extends Payload {


    private String videoFilename;


    public String getVideoDescriptionFilename() {
        return videoDescriptionFilename;
    }

    public void setVideoDescriptionFilename(String videoDescriptionFilename) {
        this.videoDescriptionFilename = videoDescriptionFilename;
    }

    private String videoDescriptionFilename;


    public String getVideoFilename() {
        return videoFilename;
    }

    public void setVideoFilename(String videoFilename) {
        this.videoFilename = videoFilename;
    }



    public static VideoPayload parsePayloadFromString(String s, String commonFilePrefix, String languageCode){
        String[] parts = s.split(SEPERATOR_CHAR);
       VideoPayload newVideoPayload = new VideoPayload();

        String[] fnameParts = parts[0].split("\\.");
        newVideoPayload.setVideoFilename(fnameParts[0]);
        newVideoPayload.setVideoDescriptionFilename(commonFilePrefix+languageCode+parts[1]);

        return newVideoPayload;

    }

    public String getEncodedString(){


        return getVideoFilename()+SEPERATOR_CHAR+getVideoDescriptionFilename();
    }

    public String toString(){
        return "VIDEO PAYLOAD IS: " +getVideoFilename();
    }
}
