package davidmcgookin.net.explore.util;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Class to help deal with LatLng and Location conversions in our code
 * Created by davidmcgookin on 18/02/16.
 *
 */
public class GoogleLocationUtilitites {



    public static Location locationFromLatLng(LatLng ll, String provider){
        Location loc = new Location(provider);
        loc.setLatitude(ll.latitude);
        loc.setLongitude(ll.longitude);
        return loc;
    }

    public static LatLng latLngFromLocation(Location l){
        LatLng ll = new LatLng(l.getLatitude(), l.getLongitude());
        return ll;
    }
}
