package davidmcgookin.net.explore.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import davidmcgookin.net.explore.R;

/**
 * *Walking sonifier that uses a step detector and sound id to sonify walking
 * Created by davidmcgookin on 16/02/16.
 */
public class StepDetectorWalkingSonifier extends WalkingSonifier implements SensorEventListener{

    private SensorManager mSensorManager;
    private Sensor mSensor;
    private final String TAG = "stepDetectWalkSonifier";


    private SoundPool mSoundPool;
    private int mStepSoundID;
    private boolean mSoundLoaded = false;
    private Context mContext;

    //takes a context and a  to play as the step sound
    public StepDetectorWalkingSonifier(Context c, String pathToStepSound) {
        mContext = c;
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        for (int i = 0; i < deviceSensors.size(); i++) {
            Sensor s = deviceSensors.get(i);
            Log.v(TAG, s.toString());

        }


        //Load the step sounds
        mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                mSoundLoaded = true;
                Log.v(TAG,"Sounds LOAD COMPLETE");
            }
        });

        AssetManager am = mContext.getAssets();

        try {
            mStepSoundID = mSoundPool.load(am.openFd(pathToStepSound), 1);

        }catch(IOException e){
                Log.e(TAG,"Error Fail load file: "+pathToStepSound);
            }

    }

    @Override
    public void startWalkingSonifier() {
        //get the step detector and register us

        if (mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
            Log.v(TAG, "HAVE STEP DETECTOR");
        } else {
            Log.v(TAG, "NO STEP DETECTOR AVAILABLE");
        }

    }

    @Override
    public void stopWalkingSonifier() {
            mSensorManager.unregisterListener(this);
    }

    @Override
    public void cleanup() {
         mSensorManager.unregisterListener(this);
            mSoundPool.release();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.v(TAG, "Step Happened"+ event.toString());
        if (mSoundLoaded) {
            mSoundPool.play(mStepSoundID, 0.9f, 0.9f, 1, 0, 1f);
            Log.e(TAG, "Played sound");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

            //null
    }
}
