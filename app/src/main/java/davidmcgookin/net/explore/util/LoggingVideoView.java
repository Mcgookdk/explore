package davidmcgookin.net.explore.util;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.VideoView;

/**
 * Created by davidmcgookin on 22/06/16.
 * Custom to get an interface for te event logging in the app
 */
public class LoggingVideoView extends VideoView {


    private static final String TAG = "LoggingVideoView";
    private VideoLoggingListener mListener;

    public LoggingVideoView(Context context) {
        super(context);
    }

    public LoggingVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoggingVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setPlayPauseListener(VideoLoggingListener listener) {
        mListener = listener;
    }

    @Override
    public void pause() {
        super.pause();
        if (mListener != null) {
            mListener.videoDidPause();
        }
    }

    @Override
    public void start() {
        super.start();
        if (mListener != null) {
            mListener.videoDidPlay();
        }
    }

    @Override
    public void resume() {
        super.resume();
        Log.v(TAG, "RESUME");
    }

    @Override
    public void stopPlayback() {
        super.stopPlayback();
        Log.v(TAG, "STOP PLAYBACK");
    }

    @Override
    public void suspend() {
        super.suspend();
        Log.v(TAG, "SUSPEND");
    }


    public interface VideoLoggingListener {
        public void videoDidPlay();

        public void videoDidPause();
    }
}