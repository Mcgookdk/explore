package davidmcgookin.net.explore.TriggeredContentActivitites;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

import davidmcgookin.net.explore.ExploreService;
import davidmcgookin.net.explore.R;
import davidmcgookin.net.explore.util.ApplicationEventLogger;
import davidmcgookin.net.explore.util.AudioPayload;
import davidmcgookin.net.explore.util.ImagePayload;
import davidmcgookin.net.explore.util.TriggeredContent;
import davidmcgookin.net.explore.util.TypefaceSpan;
import uk.co.senab.photoview.PhotoViewAttacher;

public class AudioContentActivity extends AppCompatActivity {

    private static final String TAG = "AudioContentActivity";

    //To communicate and Bind with the Service
    private boolean mIsBound = false;
    private ExploreService mExploreService = null;
    private TriggeredContent mTriggeredContent;
    private MediaPlayer mMediaPlayer;
    private ProgressBar mPlaybackProgressbar;
    private boolean mMediaPlayerIsAvailable = false;
    private Timer mMediaUpdateTimer;
    private boolean mWasLaunchedFromService = false;
    private ImageButton mPlayPauseButton = null;

    private void setTitle(String str){
        //Update the look and feel of the view to reflect the Year
        SpannableString s = new SpannableString(str);
        String fontName = mExploreService.getFontnameForCurrentYear();
        s.setSpan(new TypefaceSpan(this, fontName), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

// Update the action bar title with the TypefaceSpan instance
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(s);
    }

    private void updateLookAndFeel(){

        setTitle(mTriggeredContent.getTitle());
        ActionBar actionBar = getSupportActionBar();
        //update the background color
        ColorDrawable colorDrawable = new ColorDrawable(mExploreService.getColorForCurrentYear());
        actionBar.setBackgroundDrawable(colorDrawable);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_audio_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        String s = i.getStringExtra(TriggeredContent.ENCODED_TRIGGERED_CONTENT_STRING);

        if(i.getExtras().containsKey(ExploreService.TRIGERED_CONTENT_LAUNCED_FROM_SERVICE)){
            mWasLaunchedFromService = true;
        }


        Log.v(TAG, s);
        mTriggeredContent = TriggeredContent.unencodeFromString(s);//,"",""); //we have no prefix to add to the filenames
        AudioPayload ip = (AudioPayload)mTriggeredContent.getPayload();
        Log.v(TAG, "STRING IS " + mTriggeredContent.toString());
        TextView tv =  (TextView) findViewById(R.id.audioDetailsTextView);
        BufferedReader reader = null;
        String txt = "";
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open(ip.getDescriptiveTextFilename())));

            // do reading, usually loop until end of file reading
            String line;

            while ((line = reader.readLine()) != null) {
                Log.v(TAG, "Parsing Line");
                txt = txt+"\n"+line; //add the newline back in

            }
        } catch (IOException e) {
            Log.v(TAG, "IO Exception reading the audio content descriptive text");
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.v(TAG, "IO Exception reading the audio content descriptive text");
                }
            }
        }
        tv.setText(txt);



        Intent intent = new Intent(this, ExploreService.class);
        bindService(intent, mConnection, 0);

         mPlayPauseButton = (ImageButton) findViewById(R.id.playPauseButton);
        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   if(mMediaPlayer.isPlaying()){
                                                       mMediaPlayer.pause();
                                                       mPlayPauseButton.setImageResource(R.drawable.play);
                                                       mExploreService.logEvent(System.currentTimeMillis(), ApplicationEventLogger.USER_STOPPED_AUDIO,"User started playing audio: "+mTriggeredContent.getId());

                                                   }else if(!mMediaPlayer.isPlaying() && mMediaPlayerIsAvailable){
                                                       mMediaPlayer.start();
                                                       mPlayPauseButton.setImageResource(R.drawable.pause);
                                                       mExploreService.logEvent(System.currentTimeMillis(), ApplicationEventLogger.USER_STARTED_AUDIO,"User started playing audio: "+mTriggeredContent.getId());

                                                   }
                                               }
                                           }

        );
        mPlaybackProgressbar = (ProgressBar) findViewById(R.id.audioProgressBar);
        mPlaybackProgressbar.setMax(100);
        mPlaybackProgressbar.setProgress(5);
        mPlaybackProgressbar.setIndeterminate(false);

        try {
        Drawable bitmap   = Drawable.createFromStream(getAssets().open(ip.getImageFilename()), null);
        //= getResources().getDrawable(R.drawable.tram);
        ImageView  imageView = (ImageView)findViewById(R.id.audioImage);
            imageView.setImageDrawable(bitmap);


            AssetFileDescriptor afd = this.getAssets().openFd(ip.getAudioFilename());


            mMediaPlayer = new MediaPlayer();

            mMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
           mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.prepare();
            mMediaPlayerIsAvailable = true;

            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.v(TAG, "COMPLETED");
                    //double check this is also not called when activity goes offscreen
                    mExploreService.logEvent(System.currentTimeMillis(), ApplicationEventLogger.AUDIO_ENDED_PLAYING,"Audio Ended Naturally: "+mTriggeredContent.getId());

                    mPlayPauseButton.setImageResource(R.drawable.play);


                }
            });

        }catch(Exception e){
            Log.e(TAG, "IOException Occured when loading file " + e.getMessage());

        }

    //Only setup the timer here
        mMediaUpdateTimer = new Timer();
        mMediaUpdateTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        int audioProgress = mMediaPlayer.getCurrentPosition() / (mMediaPlayer.getDuration()/100);
                      //  int audioProgressPercent = (int) (audioProgress * 10.0);
                        mPlaybackProgressbar.setProgress(audioProgress);
                        Log.v(TAG, "AUDIO PLAYBACK AT " + audioProgress + "%" + audioProgress);
                    }
                });


            }
        },
        1000, 1000);

        //set the title
        getSupportActionBar().setTitle(mTriggeredContent.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if(!mWasLaunchedFromService) { //Then we just pop it off the stack rather than going ot the home activity
            switch (item.getItemId()) {
                // Respond to the action bar's Up/Home button
                case android.R.id.home:
                    mExploreService.logEvent(System.currentTimeMillis(), ApplicationEventLogger.USER_TERMINATED_TRIGGERED_CONTENT_ACTIVITY_BY_PRESSING_UP,"User terminated activity for Audio Triggered Content: "+mTriggeredContent.getId());

                    finish();
                    return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void onPause() {
        super.onPause();
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }


        if (mIsBound) {
            mExploreService.undipAudio(); //images don't dip audio
            if (mWasLaunchedFromService) {
                mExploreService.triggeredActivityReportsDeActivation();
            }else{
                mExploreService.triggeredActivityManuallyGeneratedReportsDeActivation(mTriggeredContent);
            }

            unbindService(mConnection);
            mIsBound = false; //make sure this is false. Can't rely on it happening in onServiceDisconnected

        }

        mMediaUpdateTimer.cancel(); //stop the timer as it keeps running

        finish(); //We want this activity to go away now
    }
   // protected void onStop() {

     //   Log.v(TAG,"Activity Stopped");
       // super.onStop();
       // m/MediaUpdateTimer.cancel(); //stop the timer as it keeps running


    //}

    /**
     * Defines callbacks for service binding and unbinding events, passed to
     * bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get
            // LocalService instance
            ExploreService.ExploreServiceBinder binder = (ExploreService.ExploreServiceBinder) service;
            mExploreService = binder.getService();
            mIsBound = true;
            Log.v(TAG, "Service Bound Sucessfully");
            //check if the service is already running.
            if (mWasLaunchedFromService){
                mExploreService.triggeredActivityReportsActivation(); //Call the service to say we have been activited and can be cleared.
        }
            mExploreService.dipAudio(); //Images fon't dip audio
            updateLookAndFeel(); //Set the font and colors

        }

        public void onServiceDisconnected(ComponentName arg0) {
            mIsBound = false;
            Log.v(TAG, "Service Did Not Bind");
        }
    };


}


