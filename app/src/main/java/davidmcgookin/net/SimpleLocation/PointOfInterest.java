package davidmcgookin.net.SimpleLocation;

import android.location.Location;


/*
 * Class to Represent a Point Of Interest as expected by the GPXLogger Class
 *  David McGookin
 *  13th June 2014 
 */

public class PointOfInterest {

	private Location location;
	private String name;
	
	
	public PointOfInterest(Location location, String name){
		this.location = location;
		this.name = name;
	}
	
	
	public String getName(){
		return name;
	}
	
	

	public Location getLocation() {
		return location;
	}
	
}
